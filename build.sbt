enablePlugins(GitVersioning, GitBranchPrompt)

ThisBuild / organization := "dev.drrzmr"
ThisBuild / scalaVersion := "2.13.1"
ThisBuild / name := "xqma"

dependencyUpdatesFilter -= moduleFilter(organization = "org.scala-lang")

lazy val base = (project in file("modules/base"))
  .enablePlugins(BuildInfoPlugin)
  .settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "build",
    buildInfoObject := "Info",
    buildInfoOptions += BuildInfoOption.BuildTime
  )

lazy val root = (project in file("modules/root"))
  .settings(
    name := "xqma",
    resolvers ++= Seq(
      Resolvers.confluent
    ),
    libraryDependencies ++= Seq(
      Dependencies.avro,
      Dependencies.avro4sCore,
      Dependencies.kafkaAvroSerializer,
      Dependencies.kafkaClients,
      Dependencies.config,
      Dependencies.scopt,
      Dependencies.json4sNative,
      Dependencies.logbackClassic,
      Dependencies.scalaLogging,
      /* tests */
      Dependencies.scalactic,
      Dependencies.scalatest
    )
  )
  .dependsOn(base)
  .aggregate(base)

lazy val kamig = (project in file("modules/kamig"))
  .settings(
    name := "kamig",
    Compile / mainClass := Some("dev.drrzmr.xqma.kamig.Main"),
    assembly / mainClass := Some("dev.drrzmr.xqma.kamig.Main"),
    libraryDependencies ++= Seq(
      Dependencies.scopt,
      Dependencies.config,
      Dependencies.akkaStream,
      Dependencies.akkaHttp,
      Dependencies.akkaActorTyped,
      Dependencies.akkaClusterShardingTyped,
      Dependencies.akkaSerializationJackson,
      Dependencies.akkaSlf4j,
      Dependencies.logbackClassic,
      Dependencies.akkaHttpCirce,
      Dependencies.circeCore,
      Dependencies.circeGeneric,
      Dependencies.circeParser
    )
  )
  .dependsOn(base)
  .aggregate(base)

lazy val Resolvers = new {
  val confluent = "io.confluent".at("https://packages.confluent.io/maven/")
}

lazy val Dependencies = new {
  val avro                     = "org.apache.avro"            % "avro"                         % Versions.avro
  val avro4sCore               = "com.sksamuel.avro4s"        %% "avro4s-core"                 % Versions.avro4s
  val kafkaClients             = "org.apache.kafka"           % "kafka-clients"                % Versions.kafka
  val kafkaAvroSerializer      = "io.confluent"               % "kafka-avro-serializer"        % Versions.confluent
  val logbackClassic           = "ch.qos.logback"             % "logback-classic"              % Versions.logbackClassic
  val scalaLogging             = "com.typesafe.scala-logging" %% "scala-logging"               % Versions.scalaLogging
  val scopt                    = "com.github.scopt"           %% "scopt"                       % Versions.scopt
  val config                   = "com.typesafe"               % "config"                       % Versions.config
  val json4sNative             = "org.json4s"                 %% "json4s-native"               % Versions.json4s
  val akkaActorTyped           = "com.typesafe.akka"          %% "akka-actor-typed"            % Versions.akka
  val akkaClusterShardingTyped = "com.typesafe.akka"          %% "akka-cluster-sharding-typed" % Versions.akka
  val akkaSerializationJackson = "com.typesafe.akka"          %% "akka-serialization-jackson"  % Versions.akka
  val akkaStream               = "com.typesafe.akka"          %% "akka-stream"                 % Versions.akka
  val akkaSlf4j                = "com.typesafe.akka"          %% "akka-slf4j"                  % Versions.akka
  val akkaHttp                 = "com.typesafe.akka"          %% "akka-http"                   % Versions.akkaHttp
  val akkaHttpCirce            = "de.heikoseeberger"          %% "akka-http-circe"             % Versions.akkaHttpCirce
  val circeCore                = "io.circe"                   %% "circe-core"                  % Versions.circe
  val circeGeneric             = "io.circe"                   %% "circe-generic"               % Versions.circe
  val circeParser              = "io.circe"                   %% "circe-parser"                % Versions.circe

  /* tests */
  val scalactic = "org.scalactic" %% "scalactic" % Versions.scalatest
  val scalatest = "org.scalatest" %% "scalatest" % Versions.scalatest % "test"
}

lazy val Versions = new {
  val avro           = "1.9.2"
  val avro4s         = "3.0.9"
  val scopt          = "4.0.0-RC2"
  val json4s         = "3.6.7"
  val kafka          = "2.4.1"
  val confluent      = "5.4.1"
  val config         = "1.4.0"
  val scalatest      = "3.0.8"
  val logbackClassic = "1.2.3"
  val scalaLogging   = "3.9.2"
  val akka           = "2.6.4"
  val akkaHttp       = "10.1.11"
  val akkaHttpCirce  = "1.31.0"
  val circe          = "0.12.3"
}

addCommandAlias("format", "all scalafmtSbt scalafmt test:scalafmt")
addCommandAlias("checkFormat", "all scalafmtSbtCheck scalafmtCheckAll test:scalafmtCheckAll")
