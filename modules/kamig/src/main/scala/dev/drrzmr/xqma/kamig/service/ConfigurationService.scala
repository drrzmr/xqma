package dev.drrzmr.xqma.kamig.service

import scala.util.Failure
import scala.util.Success
import scala.util.Try
import com.typesafe.config.{ConfigFactory => LightbendConfigFactory}
import com.typesafe.config.{Config => LightbendConfig}
import dev.drrzmr.xqma.kamig.domain.config
import dev.drrzmr.xqma.kamig.domain.exception.InvalidConfigException

trait ConfigurationService {
  import ConfigurationService.load

  lazy val configuration: config.Config = {
    load() match {
      case Success(value)     => value
      case Failure(exception) => throw exception
    }
  }
}

object ConfigurationService {

  private def load(): Try[config.Config] = Try {
    val c: LightbendConfig = LightbendConfigFactory.load().resolve()

    config.Config(
      src = config.Cluster(
        broker = loadBroker(c, "xqma.kamig.src.broker").get,
        schemaRegistry = loadSchemaRegistry(c, "xqma.kamig.src.schema-registry").get
      ),
      dst = config.Cluster(
        broker = loadBroker(c, "xqma.kamig.dst.broker").get,
        schemaRegistry = loadSchemaRegistry(c, "xqma.kamig.dst.schema-registry").get
      ),
    )
  }

  private def loadBroker(c: LightbendConfig, path: String): Try[config.Broker] = Try {
    val subTree = c.getConfig(path)
    config.Broker(
      list = subTree.getString("list").split(",").toSet,
      username = subTree.getString("username"),
      password = subTree.getString("password"),
      securityProtocol = {
        val key = "security-protocol"
        val sp  = subTree.getString(key)
        if (config.BrokerSecurityProtocolMap.contains(sp)) config.BrokerSecurityProtocolMap(sp)
        else throw InvalidConfigException(s"Invalid config: $path.$key = $sp")
      }
    )
  }

  private def loadSchemaRegistry(c: LightbendConfig, path: String): Try[config.SchemaRegistry] = Try {
    val subTree = c.getConfig(path)
    config.SchemaRegistry(
      url = subTree.getString("url"),
      username = subTree.getString("username"),
      password = subTree.getString("password")
    )
  }
}
