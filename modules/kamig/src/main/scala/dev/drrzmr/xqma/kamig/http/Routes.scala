package dev.drrzmr.xqma.kamig.http

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.util.Timeout
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

import dev.drrzmr.xqma.kamig.extension.json
import dev.drrzmr.xqma.kamig.actor

object Routes {
  implicit val timeout: Timeout = 1 second

  def apply(system: ActorSystem[_]): Routes = new Routes(ClusterSharding(system))
}

class Routes(sharding: ClusterSharding) extends json.Encoders {
  import Routes._

  private def subjectQuery(subject: String): Future[actor.Subject.QueryResult] = {
    val ref = sharding.entityRefFor(actor.Subject.TypeKey, subject)
    ref.ask(actor.Subject.Query)
  }

  val mainRoute: Route =
    path("v1" / "kamig" / Segment) { subject: String =>
      get {
        complete(subjectQuery(subject))
      }
    }

}
