package dev.drrzmr.xqma.kamig

import io.circe.Json

package object domain {
  final case class FileLine(key: SchemaRegistryKafkaKey, value: String)
  final case class Settings(programName: String, version: String)

  sealed trait SchemaRegistryKeySubject { val subject: String }
  sealed trait SchemaRegistryKafkaKey
  final case class SchemaRegistryKafkaKeySchema(subject: String, version: Int, magic: Int)
      extends SchemaRegistryKafkaKey
      with SchemaRegistryKeySubject
  final case class SchemaRegistryKafkaKeyConfig(subject: String, magic: Int)
      extends SchemaRegistryKafkaKey
      with SchemaRegistryKeySubject
  final case class SchemaRegistryKafkaKeyDeleteSubject(subject: String, magic: Int)
      extends SchemaRegistryKafkaKey
      with SchemaRegistryKeySubject

  final case class SchemaRegistryKafkaKeyNoop(magic: Int) extends SchemaRegistryKafkaKey

  sealed trait SchemaRegistryValue
  final case class SchemaRegistryValueSchema(subject: String, version: Int, id: Int, schema: Json)
      extends SchemaRegistryValue
  final case class SchemaRegistryValueConfig(compatibilityLevel: CompatibilityLevel) extends SchemaRegistryValue
  final case class SchemaRegistryValueDeleteSubject(subject: String, version: Int)   extends SchemaRegistryValue
  final case object SchemaRegistryValueNull                                          extends SchemaRegistryValue

  final case class SchemaRegistryInfo(key: domain.SchemaRegistryKeySubject, value: domain.SchemaRegistryValue)

  sealed trait CompatibilityLevel { val name: String }
  case object CompatibilityLevelNone     extends CompatibilityLevel { val name = "NONE"     }
  case object CompatibilityLevelForward  extends CompatibilityLevel { val name = "FORWARD"  }
  case object CompatibilityLevelBackward extends CompatibilityLevel { val name = "BACKWARD" }

}
