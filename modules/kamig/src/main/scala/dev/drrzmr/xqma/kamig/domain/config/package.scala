package dev.drrzmr.xqma.kamig.domain

package object config {

  sealed trait BrokerSecurityProtocol
  case object EMPTY    extends BrokerSecurityProtocol
  case object SASL_SSL extends BrokerSecurityProtocol

  val BrokerSecurityProtocolMap = Map(
    ""         -> EMPTY,
    "SASL_SSL" -> SASL_SSL
  )

  case class Broker(list: Set[String], username: String, password: String, securityProtocol: BrokerSecurityProtocol)
  case class SchemaRegistry(url: String, username: String, password: String)
  case class Cluster(broker: Broker, schemaRegistry: SchemaRegistry)
  case class Config(src: Cluster, dst: Cluster)
}
