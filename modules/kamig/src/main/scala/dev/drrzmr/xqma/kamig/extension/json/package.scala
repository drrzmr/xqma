package dev.drrzmr.xqma.kamig.extension

import io.circe.DecodingFailure
import io.circe.HCursor
import io.circe.Json
import io.circe.{Decoder => D}
import io.circe.{Encoder => E}
import io.circe.syntax._
import io.circe.generic.semiauto._
import io.circe.parser.parse

import scala.util.Failure
import scala.util.Success
import scala.util.Try
import dev.drrzmr.xqma.kamig.domain
import dev.drrzmr.xqma.kamig.actor

package object json {

  trait Helpers {

    def tryDecode[T](raw: String)(implicit decoder: io.circe.Decoder[T]): Try[T] =
      parse(raw).flatMap(obj => obj.as[T]) match {
        case Left(error)  => Failure(error)
        case Right(value) => Success(value)
      }
  }

  trait DomainEncoders {
    implicit val schemaEncoder: E[domain.SchemaRegistryKafkaKeySchema]               = deriveEncoder
    implicit val configEncoder: E[domain.SchemaRegistryKafkaKeyConfig]               = deriveEncoder
    implicit val deleteSubjectEncoder: E[domain.SchemaRegistryKafkaKeyDeleteSubject] = deriveEncoder
    implicit val noopEncoder: E[domain.SchemaRegistryKafkaKeyNoop]                   = deriveEncoder
    implicit val keyEncoder: E[domain.SchemaRegistryKafkaKey] = {
      case obj: domain.SchemaRegistryKafkaKeySchema        => obj.asJson
      case obj: domain.SchemaRegistryKafkaKeyConfig        => obj.asJson
      case obj: domain.SchemaRegistryKafkaKeyDeleteSubject => obj.asJson
      case obj: domain.SchemaRegistryKafkaKeyNoop          => obj.asJson
    }

    implicit val subjectEncoder: E[domain.SchemaRegistryKeySubject] = {
      case obj: domain.SchemaRegistryKafkaKeySchema        => obj.asJson
      case obj: domain.SchemaRegistryKafkaKeyConfig        => obj.asJson
      case obj: domain.SchemaRegistryKafkaKeyDeleteSubject => obj.asJson
    }

    implicit val valueSchemaEncoder: E[domain.SchemaRegistryValueSchema]               = deriveEncoder
    implicit val valueConfigEncoder: E[domain.SchemaRegistryValueConfig]               = deriveEncoder
    implicit val valueDeleteSubjectEncoder: E[domain.SchemaRegistryValueDeleteSubject] = deriveEncoder
    implicit val valueEncoder: E[domain.SchemaRegistryValue] = {
      case domain.SchemaRegistryValueNull               => Json.Null
      case obj: domain.SchemaRegistryValueSchema        => obj.asJson
      case obj: domain.SchemaRegistryValueConfig        => obj.asJson
      case obj: domain.SchemaRegistryValueDeleteSubject => obj.asJson
    }

    implicit val compatibilityLevelEncoder: E[domain.CompatibilityLevel] = {
      case domain.CompatibilityLevelNone     => Json.fromString(domain.CompatibilityLevelNone.name)
      case domain.CompatibilityLevelBackward => Json.fromString(domain.CompatibilityLevelBackward.name)
      case domain.CompatibilityLevelForward  => Json.fromString(domain.CompatibilityLevelForward.name)
    }

    implicit val infoEncoder: E[domain.SchemaRegistryInfo] = deriveEncoder

    implicit val fileLineEncoder: E[domain.FileLine] = deriveEncoder
  }

  trait DomainDecoders {
    implicit val schemaDecoder: D[domain.SchemaRegistryKafkaKeySchema]               = deriveDecoder
    implicit val configDecoder: D[domain.SchemaRegistryKafkaKeyConfig]               = deriveDecoder
    implicit val deleteSubjectDecoder: D[domain.SchemaRegistryKafkaKeyDeleteSubject] = deriveDecoder
    implicit val noopDecoder: D[domain.SchemaRegistryKafkaKeyNoop]                   = deriveDecoder
    implicit val keyDecoder: D[domain.SchemaRegistryKafkaKey] = (c: HCursor) => {
      c.get[String]("keytype") match {
        case Right("SCHEMA")         => c.value.as[domain.SchemaRegistryKafkaKeySchema]
        case Right("CONFIG")         => c.value.as[domain.SchemaRegistryKafkaKeyConfig]
        case Right("DELETE_SUBJECT") => c.value.as[domain.SchemaRegistryKafkaKeyDeleteSubject]
        case Right("NOOP")           => c.value.as[domain.SchemaRegistryKafkaKeyNoop]
        case Right(keyType)          => Left(DecodingFailure(s"unknown $keyType keytype", c.history))
        case Left(error)             => Left(error)
      }
    }

    implicit val compatibilityLevelDecoder: D[domain.CompatibilityLevel] = (c: HCursor) =>
      c.value.asString match {
        case Some(domain.CompatibilityLevelForward.name)  => Right(domain.CompatibilityLevelForward)
        case Some(domain.CompatibilityLevelNone.name)     => Right(domain.CompatibilityLevelNone)
        case Some(domain.CompatibilityLevelBackward.name) => Right(domain.CompatibilityLevelBackward)
        case _ =>
          Left(DecodingFailure(s"unknown compatibility level: ${c.value.asString.getOrElse("")}", c.history))
    }

    implicit val schemaValueDecoder: D[domain.SchemaRegistryValueSchema]               = deriveDecoder
    implicit val configValueDecoder: D[domain.SchemaRegistryValueConfig]               = deriveDecoder
    implicit val deleteSubjectValueDecoder: D[domain.SchemaRegistryValueDeleteSubject] = deriveDecoder

  }

  trait ProtocolEncoders extends DomainEncoders {
    implicit val queryResultEncoder: E[actor.Subject.QueryResult] = deriveEncoder
  }

  trait ProtocolDecoders extends DomainDecoders
  trait Encoders         extends DomainEncoders with ProtocolEncoders
  trait Decoders         extends DomainDecoders with ProtocolDecoders
}
