package dev.drrzmr.xqma.kamig.stream

import java.nio.file.Paths

import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success
import scala.util.Try
import akka.Done
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.cluster.sharding.typed.scaladsl.EntityRef
import akka.stream.IOResult
import akka.stream.scaladsl.FileIO
import akka.stream.scaladsl.Framing
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import akka.util.ByteString
import akka.util.Timeout
import dev.drrzmr.xqma.kamig.actor.Subject
import io.circe.parser.parse
import dev.drrzmr.xqma.kamig.domain.FileLine
import dev.drrzmr.xqma.kamig.domain.SchemaRegistryKafkaKey
import dev.drrzmr.xqma.kamig.domain.SchemaRegistryKeySubject
import dev.drrzmr.xqma.kamig.extension.json

import scala.language.postfixOps

class LoadFile(fn: String)(implicit system: ActorSystem[_]) extends json.DomainDecoders {

  import LoadFile._

  val sharding: ClusterSharding = ClusterSharding(system)

  private def schemaCommand(key: SchemaRegistryKeySubject, value: String): Future[Subject.LineEvent] =
    sharding
      .entityRefFor(Subject.TypeKey, key.subject)
      .ask(Subject.SchemaCommand(key, value, _))

  def run(): Future[Done] = {

    val loaded = FileIO
      .fromPath(Paths.get(fn))
      .via(Framing.delimiter(ByteString("\n"), maximumFrameLength = 15000, allowTruncation = false))
      .map(frame => frame.utf8String.split('\t'))
      .map(line => if (line.length == 2) line else throw new RuntimeException(s"invalid file line: $line"))
      .map(line =>
        parse(line(0)).flatMap(_.as[SchemaRegistryKafkaKey]) match {
          case Left(error) => throw new RuntimeException(error)
          case Right(key)  => Success(FileLine(key, line(1)))
      })
      .recover {
        case e: RuntimeException =>
          e.printStackTrace()
          Failure(e)
      }

    val filtered = loaded.collect {
      case Success(FileLine(key: SchemaRegistryKeySubject, value)) => Some((key, value))
      case _                                                       => None
    }.filter(_.isDefined)
      .map(_.get)

    val response: Source[String, Future[IOResult]] = filtered
      .mapAsync(1) {
        case (key, value) => schemaCommand(key, value)
      }
      .map {
        case Subject.ValueFailed(error)  => throw error
        case Subject.LineAdded(entityId) => entityId
      }

    response.runForeach((_: String) => Done)

  }
}

object LoadFile {
  import scala.concurrent.duration._

  def apply(fn: String)(system: ActorSystem[_]): LoadFile = new LoadFile(fn)(system)

  implicit val timeout: Timeout = 1 second
}
