package xqma

import org.scalatest.FlatSpec
import org.scalatest.Matchers

import xqma.tools.Settings

class SettingsSuite extends FlatSpec with Matchers {

  "A Settings" should "has defaults loaded" in {
    val default = Settings.default
    default.schemaRegistry.url should be("http://localhost:8081")
  }
}
