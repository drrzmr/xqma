package xqma.tools

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.Logger
import com.typesafe.scalalogging.LazyLogging
import org.slf4j.LoggerFactory

trait Logging extends LazyLogging

object Logging {

  val INFO  = "info"
  val DEBUG = "debug"
  val WARN  = "warn"
  private val logger = LoggerFactory
    .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME)
    .asInstanceOf[Logger]

  def setLevel(level: String): Unit =
    logger.setLevel(level match {
      case DEBUG => Level.DEBUG
      case INFO  => Level.INFO
      case WARN  => Level.WARN
      case _     => throw new IllegalArgumentException(s"Unsupported log level: $level")
    })
}
