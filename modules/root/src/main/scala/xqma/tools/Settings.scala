package xqma.tools

import java.nio.file.Paths

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import dev.drrzmr.xqma.BuildInfo

case class Settings(config: Config) {

  object schemaRegistry {
    val url: String = config.getString(Settings.SCHEMA_REGISTRY_URL)
  }

  val logLevel: String = config.getString(Settings.LOG_LEVEL)
}

object Settings {
  private val LOG_LEVEL           = "log.level"
  private val SCHEMA_REGISTRY_URL = "schema.registry.url"

  val name: String      = "kamig"
  val version: String   = BuildInfo.version
  val default: Settings = Settings(ConfigFactory.load("default"))

  def apply(filename: String): Settings = {
    val file   = Paths.get(filename).toFile
    val config = ConfigFactory.parseFile(file).withFallback(default.config)
    Settings(config)
  }
}
