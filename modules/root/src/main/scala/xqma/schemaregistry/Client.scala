package xqma.schemaregistry

import java.util

import io.confluent.kafka.schemaregistry.client.rest.RestService
import io.confluent.kafka.schemaregistry.client.rest.entities.Config
import io.confluent.kafka.schemaregistry.client.rest.entities.requests.ConfigUpdateRequest
import io.confluent.kafka.schemaregistry.client.SchemaMetadata
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
import org.apache.avro.Schema

class Client(baseUrl: String) extends SchemaRegistryClient {

  private lazy val restService = new RestService(baseUrl)

  override def getAllVersions(s: String): util.List[Integer]                                      = ???
  override def deleteSubject(map: util.Map[String, String], s: String): util.List[Integer]        = ???
  override def deleteSubject(s: String): util.List[Integer]                                       = ???
  override def deleteSchemaVersion(s: String, s1: String): Integer                                = ???
  override def deleteSchemaVersion(map: util.Map[String, String], s: String, s1: String): Integer = ???
  override def register(s: String, schema: Schema, i: Int, i1: Int): Int                          = ???
  override def getAllSubjectsById(i: Int): util.Collection[String]                                = ???
  override def setMode(s: String): String                                                         = ???
  override def setMode(s: String, s1: String): String                                             = ???
  override def getMode: String                                                                    = ???
  override def getMode(s: String): String                                                         = ???
  override def reset(): Unit                                                                      = ???
  override def getId(subject: String, schema: Schema): Int = synchronized {
    getIdFromRegistry(subject, schema)
  }

  private def getIdFromRegistry(subject: String, schema: Schema): Int =
    restService
      .lookUpSubjectVersion(schema.toString, subject)
      .getId
      .intValue

  override def getVersion(subject: String, schema: Schema): Int = synchronized {
    getVersionFromRegistry(subject, schema)
  }

  private def getVersionFromRegistry(subject: String, schema: Schema): Int =
    restService
      .lookUpSubjectVersion(schema.toString, subject)
      .getVersion
      .intValue

  override def getAllSubjects: util.List[String] = restService.getAllSubjects()
  override def getById(id: Int): Schema          = getByID(id)
  override def getByID(id: Int): Schema = synchronized {
    getBySubjectAndID(null, id)
  }

  override def getBySubjectAndId(subject: String, id: Int): Schema = getBySubjectAndID(subject, id)
  override def getBySubjectAndID(subject: String, id: Int): Schema = synchronized {
    getSchemaByIdFromRegistry(id)
  }

  private def getSchemaByIdFromRegistry(id: Int): Schema =
    (new Schema.Parser).parse(restService.getId(id).getSchemaString)

  override def getSchemaMetadata(subject: String, version: Int): SchemaMetadata = {
    val response = restService.getVersion(subject, version)
    val id       = response.getId.intValue
    val schema   = response.getSchema
    new SchemaMetadata(id, version, schema)
  }

  override def getLatestSchemaMetadata(subject: String): SchemaMetadata = synchronized {
    val response = restService.getLatestVersion(subject)
    val id       = response.getId.intValue
    val version  = response.getVersion.intValue
    val schema   = response.getSchema
    new SchemaMetadata(id, version, schema)
  }

  override def updateCompatibility(subject: String, compatibility: String): String = {
    val response: ConfigUpdateRequest = restService.updateCompatibility(compatibility, subject)
    response.getCompatibilityLevel
  }

  override def getCompatibility(subject: String): String = {
    val response: Config = restService.getConfig(subject)
    response.getCompatibilityLevel
  }

  override def testCompatibility(subject: String, schema: Schema): Boolean =
    restService.testCompatibility(schema.toString(), subject, "latest")

  override def register(subject: String, schema: Schema): Int = synchronized {
    registerAndGetId(subject, schema)
  }

  private def registerAndGetId(subject: String, schema: Schema): Int =
    restService.registerSchema(schema.toString, subject)

}

object Client {
  def apply(baseUrl: String): Client = new Client(baseUrl)
}
