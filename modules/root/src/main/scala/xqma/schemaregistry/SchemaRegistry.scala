package xqma.schemaregistry

import java.io.File

import org.apache.avro
import org.apache.avro.Schema
import xqma.schemaregistry
import xqma.tools.Settings

import scala.collection.JavaConverters._

class SchemaRegistry(settings: Settings) {

  private val parser = new avro.Schema.Parser()
  private val client = schemaregistry.Client(settings.schemaRegistry.url)

  private def loadSchema(fn: String): Schema      = parser.parse(new File(fn))
  private def keySubject(topic: String): String   = s"$topic-key"
  private def valueSubject(topic: String): String = s"$topic-value"

  def register(topic: String, key: Option[String], value: Option[String]): (Int, Int) = {
    val keyID = key match {
      case Some(fn) =>
        client.register(
          subject = keySubject(topic),
          schema = loadSchema(fn)
        )
      case None => -1
    }

    val valueID = value match {
      case Some(fn) =>
        client.register(
          subject = valueSubject(topic),
          schema = loadSchema(fn)
        )
      case None => -1
    }

    (keyID, valueID)
  }

  def listSubjects(): List[String] = client.getAllSubjects.asScala.toList
}

object SchemaRegistry {

  def apply(settings: Settings): SchemaRegistry = new SchemaRegistry(settings)
}
