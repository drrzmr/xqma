package xqma.command

trait CommandType

object Command {
  case object None           extends CommandType
  case object SchemaRegistry extends CommandType
}
