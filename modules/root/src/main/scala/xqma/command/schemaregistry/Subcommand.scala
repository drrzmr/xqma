package xqma.command.schemaregistry

import xqma.command.SubcommandType

object Subcommand {
  case object Register     extends SubcommandType
  case object ListSubjects extends SubcommandType
}
