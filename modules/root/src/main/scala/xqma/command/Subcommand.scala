package xqma.command

trait SubcommandType

object Subcommand {
  case object None extends SubcommandType
}
