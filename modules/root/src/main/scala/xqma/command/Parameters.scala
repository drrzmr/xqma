package xqma.command

case class Parameters(command: CommandType = Command.None,
                      subcommand: SubcommandType = Subcommand.None,
                      data: Map[String, String] = Map.empty,
                      configFileName: String = "",
                      commandLine: Seq[String] = Seq.empty)
