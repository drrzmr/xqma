package xqma

import xqma.command.Command
import xqma.command.schemaregistry.{Subcommand => schemaRegistryCmd}
import xqma.tools.Logging
import xqma.tools.Result
import xqma.tools.ResultType
import xqma.command.Load
import xqma.schemaregistry.SchemaRegistry

object Main extends App with Logging {

  val cmd        = Load(args)
  val parameters = cmd.parameters
  val settings   = cmd.settings

  val result: ResultType = parameters.command match {

    case Command.SchemaRegistry =>
      parameters.subcommand match {
        case schemaRegistryCmd.Register =>
          (parameters.data.get("key"), parameters.data.get("value")) match {
            case (None, None) => tools.Result.Error("missing key or value", -1)
            case (key, value) =>
              val schemaRegistry   = SchemaRegistry(settings)
              val topic            = parameters.data("topic")
              val (keyID, valueID) = schemaRegistry.register(topic, key, value)
              Result.Ok(s"key id: $keyID, value id: $valueID")
          }

        case schemaRegistryCmd.ListSubjects =>
          val schemaRegistry = SchemaRegistry(settings)

          schemaRegistry
            .listSubjects()
            .foreach(subject => {
              println(s"$subject")
            })

          Result.SilentOk

        case _ => Result.Error("asdadas unknown command", -1)
      }
  }

  System.exit(result match {
    case Result.SilentOk => 0
    case Result.Ok(message) =>
      println(message)
      0
    case Result.Error(message, code) =>
      println(message)
      code
  })
}
